import graphene
from graphene_django import DjangoObjectType
from .models import Books


class BooksType(DjangoObjectType):
    class Meta:
        model = Books
        fields = ("id", "title", "excerpt")


# DjangoObject type will format the class into:-
'''
type Books {
    id: id
    title: String
    Excerpt: String
}
'''


class Query(graphene.ObjectType):
    all_books = graphene.List(BooksType)

    def resolve_all_books(root, info):
        return Books.objects.all()


schema = graphene.Schema(query=Query)
